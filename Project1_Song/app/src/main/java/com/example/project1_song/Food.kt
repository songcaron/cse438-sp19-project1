package com.example.project1_song

class Food{
    var foodName: String = ""
    var foodCalories: String = ""

    constructor() {}

    constructor(foodName: String, foodCalories: String){
        this.foodName = foodName
        this.foodCalories = foodCalories
    }
}