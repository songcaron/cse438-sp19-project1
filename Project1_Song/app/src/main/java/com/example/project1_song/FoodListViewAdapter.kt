package com.example.project1_song

import kotlinx.android.synthetic.main.food_item_dialog.view.*
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.app.Activity
import android.widget.BaseAdapter


class FoodListViewAdapter(private var activity: Activity, private var items: ArrayList<Food>) : BaseAdapter() {

    private class ViewHolder(row: View?) {
        var foodName: TextView? = null
        var foodCalories: TextView? = null

        init {
            this.foodName = row?.findViewById<TextView>(R.id.foodView)
            this.foodCalories = row?.findViewById<TextView>(R.id.calorieView)
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View?
        val viewHolder: ViewHolder
        if (convertView == null) {
            val inflater = activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.row, null)
            viewHolder = ViewHolder(view)
            view?.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }

        var foodItems = items[position]
        viewHolder.foodName?.text = foodItems.foodName
        viewHolder.foodCalories?.text = foodItems.foodCalories

        return view as View
    }

    override fun getItem(i: Int): Food {
        return items[i]
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }

    override fun getCount(): Int {
        return items.size
    }

}
