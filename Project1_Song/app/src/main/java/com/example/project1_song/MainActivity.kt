package com.example.project1_song

import android.app.ActionBar
import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.content.Intent
import android.widget.*
import android.widget.EditText
import android.media.MediaPlayer


import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.total_calorie_dialog.*
import kotlinx.android.synthetic.main.total_calorie_dialog.view.*
import org.w3c.dom.Text

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        setSupportActionBar(toolbar)


        val totalCalDialog = Dialog(this)
        totalCalDialog.setContentView(R.layout.total_calorie_dialog)

        val window = totalCalDialog.window
        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT)


//        //AlertDialogBuilder
//        val alertBuilder = AlertDialog.Builder(this)
//            .setView(totalCalDialog)
//            .setTitle("Enter Total Calories")
        val totalCaloriesEt = totalCalDialog.dialogCal

        //show dialog
        //val mAlertDialog = alertBuilder.show()


        totalCalDialog.dialogSubmit.setOnClickListener{

            val totalCalories = totalCaloriesEt.text.toString()
            val mp = MediaPlayer.create (this, R.raw.button)
            mp.start()
            if(totalCalories.length <= 0)
            {
                Toast.makeText(this, "Please enter a valid number", Toast.LENGTH_SHORT).show()
            }
            else
            {
                    val intent = Intent(this@MainActivity, SecondActivity::class.java)
                    intent.putExtra("totalCalories", totalCalories)
                    totalCalDialog.dismiss()
                    startActivity(intent)
            }
        }

        totalCalDialog.show()

    }

}