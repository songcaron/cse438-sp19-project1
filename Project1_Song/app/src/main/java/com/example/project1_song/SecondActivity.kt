package com.example.project1_song


import android.app.ActionBar
import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import android.widget.*
import android.widget.ListView
import kotlinx.android.synthetic.main.food_item_dialog.*
import android.graphics.Color
import android.media.MediaPlayer

class SecondActivity : AppCompatActivity() {

    var foodList = ArrayList<Food>()
    var listView: ListView? = null
    var adapter: FoodListViewAdapter? = null
    var foodCalSum: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var listView: ListView? = null
        listView = findViewById<ListView>(R.id.foodList)

        var intent = intent
        var totalCalories = intent.getStringExtra("totalCalories")
        var resultTotalCal = findViewById<TextView>(R.id.totalCalories)
        resultTotalCal.text = totalCalories
        var remTotalCal = findViewById<TextView>(R.id.inputCal)
        remTotalCal.text = totalCalories



        val foodItemBtn: Button? = findViewById(R.id.addFoodButton)


        adapter = FoodListViewAdapter(this, foodList)
        listView?.adapter = adapter
        adapter?.notifyDataSetChanged()

        foodItemBtn?.setOnClickListener {



            val foodDialog = Dialog(this)
            foodDialog.setContentView(R.layout.food_item_dialog)

            val window = foodDialog.window
            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT)
            val mp = MediaPlayer.create (this, R.raw.button)
            mp.start()



            val foodNameEt = foodDialog.foodName
            val foodCaloriesEt = foodDialog.foodCalories


            //show dialog
//            val mAlertDialog = alertBuilder.show()

            foodDialog.dialogFoodSubmit.setOnClickListener{

                val foodName = foodNameEt.text.toString()
                val foodCalories = foodCaloriesEt.text.toString()
                val mp = MediaPlayer.create (this, R.raw.button)
                mp.start()

                if (foodName.length <= 0)
                {
                    Toast.makeText(this, "Please enter a valid food", Toast.LENGTH_SHORT).show()
                }
                else if (foodCalories.length <= 0)
                {
                    Toast.makeText(this, "Please enter a valid number", Toast.LENGTH_SHORT).show()
                }
                else
                {
                    foodList.add(Food(foodName, foodCalories))

                    for(i in 1..foodCalories.length-1){

                        foodCalSum += foodCalories.toInt()
                    }


                    var updateCal = (totalCalories.toInt()- foodCalSum)
                    if(updateCal < 0)
                    {
                        remTotalCal.setTextColor(Color.RED)
                    }
                    remTotalCal.text = updateCal.toString()
                    foodDialog.dismiss()

                }

            }

            foodDialog.show()

        }
    }

}
