In this file you should include:

Any information you think we should know about your submission
* Is there anything that doesn't work? Why?
There is something off with the array iterationo when calculating the total calories remaining but I can't figure out what the error is.

* Is there anything that you did that you feel might be unclear? Explain it here.
SecondActivity has the main interface of the app because I misunderstood the PDF initially. 

A description of the creative portion of the assignment
* Describe your feature
There are sound effects when you press the buttons.
* Why did you choose this feature?
Sound adds a nice touch to this app to make it more appealing.
* How did you implement it?
I used Kotlin's Media Player import.

1. (10 / 10 Points) User can enter total calorie amount on start up
2. (8 / 8 Points) User can add new food item by name
3. (8 / 8 Points) User can add new food item by calorie
4. (4 / 4 Points) Adding new food items is done in a second activity
5. (5 / 5 Points) Calories remaining is updated with each new food item
	-4 I don't see this value anywhere
6. (5 / 5 Points) Calorie consumed is updated with each new food item
7. (10 / 10 Points) The list of food items displays foods and their respective calories amounts
8. (10 / 10 Points) Color change when calorie count becomes negative
	-2 It changes the color of calories consumbed, but not at the right time
9. (10 / 10 Points) All inputs are filtered and error messages are displayed accordingly
10. (2 / 2 Points) Code is clean and commented
11. (3 / 3 Points) App is visually appealing
12. (3 / 15 Points) Creative portion - design your own feature(s)!
	-12 I have some serious problems with this feature. First, I do not think it adds a lot of value, in fact if this were to happen with an app that I use, I probably
	wouldn't use it anymore. At the very least there needs to be a way to turn the sounds off. The other issue is that adding in what amounts to two or three lines of code
	per button press is not a sufficient creative option. I'm looking for a signification addition to the functionality of the app.

Total: 72 / 90

